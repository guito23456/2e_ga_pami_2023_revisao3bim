import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { IonicModule } from '@ionic/angular';
import { FormsModule } from '@angular/forms';

import { ProdutoPage } from './produto.page';


import { ProdutoPageRoutingModule } from './produto-routing.module';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProdutoPageRoutingModule
  ],
  declarations: [ProdutoPage],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class ProdutoPageModule {}
