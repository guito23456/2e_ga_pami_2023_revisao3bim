import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicSlides } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})







export class HomePage {



  
  itensMenuSuperior = [
    { icon: 'home-outline', texto: ' Início' },
    { icon: 'flash-outline', texto: ' Ofertas do Dia' },
    { icon: 'reload-outline', texto: ' Histórico' },
    { icon: 'construct-outline', texto: ' Contato' },
  ]

  itensMenuInferior = [
    { icon: 'shirt-outline', texto: ' Moda' },
    { icon: 'flame-outline', texto: ' Mais Vendidos' },
    { icon: 'airplane-outline', texto: ' Compra Internacional' },
    { icon: 'storefront-outline', texto: ' Lojas Oficias' },
    { icon: 'list-outline', texto: ' Categorias' }, 
  ]

  produtos = [
    {
      nome: 'Gabinete Gamer K-mex Aquário Space Atx Cg-p2r4 Sem Fans',
      descricao: ' Gabinete Compatível com placa mãe formato ATX, Micro ATX e ITX Visão Geral, Chapa lateral de vidro temperado 3mm, Filtro anti-poeira inferior e superior magnético, Painel de vidro temperado 3 mm, HD áudio',
      preco: 499,
      imagem: '/assets/home/gabinete.webp'
    },
    {
      nome: 'Mesa De Computador Escrivaninha Gamer Start Xp',
      descricao: 'A Mesa Gamer é a peça ideal para quem procura praticidade para seu ambiente, ela comporta  dois monitores e prateleiras para seu CPU e organizar pequenos objetos em sua gaveta.',
      preco: 359,
      imagem: '/assets/home/mesa.webp',
    
      
      
  
    },
    { 
      nome: 'Cadeira Gamer Hushy Preta Com Led E Bluetooth',
      descricao: ' Com esta cadeira EFA, desfrutará do conforto e bem-estar de que necessita ao longo do dia. Além disso, você pode colocá-lo em qualquer lugar da sua casa ou escritorio, pois foi projetado para se adaptar a diferentes ambientes. ',
      preco: 899,
      imagem: '/assets/home/cadeira.webp'
    },
    {
      nome: ' Fita Led ',
      descricao: ' fita de LED RGB 5050 USB, Controles direto na fita que permite você modificar a cor, intensidade e variações das luzes. O produto é ideal para quem deseja personalizar a TV, computador, monitor ou painel do carro pois você pode conectar a fita em qualquer lugar.',
      preco:99,
      imagem: '/assets/home/fita.webp'
      
    },
  ]
    slideProduto2 = [
      {
      nome: 'Mouse Gamer Logitech G203 LIGHTSYNC RGB, Efeito de Ondas de Cores, 6 Botões Programáveis e Até 8.000 DPI - Azul',
      descricao: 'O G203 LIGHTSYNC vem pronto para jogar com um sensor de 8.000 DPI e cores RGB LIGHTSYNC personalizáveis. ',
      preco: 2.75,
      imagem: '/assets/home/moouse.webp'
      }
    ]
    slideProduto1 = [
      {
      nome: 'Teclado Mecânico Gamer Razer Blackwidow V3 Green Switch, Quartz Edition Rosa',
      descricao: 'Interruptor mecânico verde razerTátil e clicável, Tamanho completo,Retroiluminação razer chroma com 16,8 milhões de opções de cores personalizáveis',
      preco: 2.759,
      imagem: '/assets/home/teclado.webp'
      }
    ]
    slideProduto3 = [
      {
      nome: 'Headset Gamer Fallen Morcego, Surround Virtual 7.1, Cancelamento de Ruído, Drivers 53mm - He-Ga-Fn-Mo',
      descricao: ' O Headset Morcego, vem com alto-falantes de alta qualidade e Surround Virtual 7.1, proporcionando uma experiência única ao jogador. É a imersão ao jogo em um novo patamar.',
      preco: 2.759,
      imagem: '/assets/home/fone.webp'
      }
    ]
    slideProduto4 = [
      {
      nome: 'Monitor Gamer Curvo Samsung Odyssey 34", ultra WQHD, 165Hz, 1ms, HDMI, DP, Freesync, G5 Preto' ,
      descricao: 'Monitor Gamer Curvo, WQHD, 165Hz, 1ms, tela ultrawide, HDMI, Display Port, Freesync Premium, preto, série G5, 165Hz de Taxa de Atualização, 1ms de Tempo de Resposta Resolução QHD, Tela Ultrawide, Painel Curvo VA Conexões: HDMI e Display Po',
      preco: 1.059,
      imagem: '/assets/home/monitor.webp'
      }
    ]
    beneficio = [
      {
      nome: 'Entrega ',
      imagem:'/assets/Icons/truck-icon.png',
      descricao: 'Frete grátis em milhões de produtos'
      }
      
    ]

    beneficio1 = [
      {
      nome: 'Disney ',
      imagem:'/assets/Icons/disney-icon.jpg',
      descricao: 'Disney+ e Star+ sem custo'
      }
      
    ]

    beneficio2 = [
      {
      nome: 'Deezer ',
      imagem:'/assets/Icons/deezer-icon.jpg',
      descricao: '12 meses grátis da Deezer Premium'
      }
      
    ]

    beneficio3 = [
      {
      nome: 'Carro ',
      imagem:'/assets/Icons/carro-icon.png',
      descricao: '60% OFF no Ultrapasse e outros benefícios'
      }
      
    ]

      constructor() {}

}
