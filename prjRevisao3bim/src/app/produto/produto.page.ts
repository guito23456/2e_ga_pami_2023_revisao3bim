import { Component, OnInit, ViewChild} from '@angular/core';

@Component({
  selector: 'app-produto',
  templateUrl: './produto.page.html',
  styleUrls: ['./produto.page.scss'],
})
export class ProdutoPage implements OnInit {

 
  @ViewChild('rating', { static: true }) ratingElement: any;
 
   ngAfterViewInit() {
 
     const starRatingElement  = this.ratingElement.nativeElement as HTMLElement;
    
     starRatingElement.style.fontSize = '0,5cm';
 
     starRatingElement.style.color = 'gold';

 

     console.log(this.ratingElement);
 
   }



  itensMenuSuperior = [
    { icon: 'home-outline', texto: ' Início' },
    { icon: 'flash-outline', texto: ' Ofertas do Dia' },
    { icon: 'reload-outline', texto: ' Histórico' },
    { icon: 'construct-outline', texto: ' Contato' },
  ]

  itensMenuInferior = [
    { icon: 'shirt-outline', texto: ' Moda' },
    { icon: 'flame-outline', texto: ' Mais Vendidos' },
    { icon: 'airplane-outline', texto: ' Compra Internacional' },
    { icon: 'storefront-outline', texto: ' Lojas Oficias' },
    { icon: 'list-outline', texto: ' Categorias' }, 
  ]

   
produto = {

  nome: 'Headset Gamer Fallen Morcego, Surround Virtual 7.1, Cancelamento de Ruído, Drivers 53mm - He-Ga-Fn-Mo',
  status: 'Novo',
  quantidadeVendas: '+100mil vendidos',
  avaliacoes: 4289,
  descricao: 'Headset Gamer Fallen Morcego A inspiração no jogo é o que nos motiva e o reconhecimento vem quando a nossa comunidade de fãs brasileiros participam dando o nome do nosso primeiro Headset! O Headset Morcego, vem com alto-falantes de alta qualidade e Surround Virtual 7.1, proporcionando uma experiência única ao jogador. É a imersão ao jogo em um novo patamar.',
  preco: 350.00,
  imagem: 'assets/home/fone.webp',

}
  constructor() { }

  ngOnInit() {
  }

}
